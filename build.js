#! /bin/node

const path = require('path');
const handlebars = require('handlebars');

// const katex = require('katex');

const texmath = require('markdown-it-texmath').use({
    renderToString: function(tex, { displayMode }) {
        const delim = displayMode ? '$$$' : '$'; // Bug????
        return `${delim}${tex}${delim}`;
    }
});

const markdown = require('markdown-it')({
  html: true,
  typographer: true
}).use(texmath, {
    delimiters: 'dollars',
    macros: { "\\RR": "\\mathbb{R}" }
});

const yaml = require("js-yaml");
const fs = require("fs");

handlebars.registerHelper('markdown', function (markdownSource) {
    return markdown.render(markdownSource);
})

handlebars.registerHelper('markdown_inline', function (markdownSource) {
    return markdown.renderInline(markdownSource);
})

// First command line argument
const FILE = process.argv[2];
const TEMPLATE = handlebars.compile(fs.readFileSync('template.hbs', 'utf-8'));

const FILE_NAME = path.basename(FILE, '.yaml');

/**
 * This function takes a yaml structured list of propositions, definitions, 
 * etc and outputs a rendered html 
 */
function processYaml2HtmlV1(context) {
    return TEMPLATE({
        ...context,
        path: FILE_NAME
    });
}

const source = fs.readFileSync(FILE, 'utf-8');
const context = yaml.safeLoad(source);

const htmlOutput = processYaml2HtmlV1(context);

fs.writeFileSync(`out/${FILE_NAME}.html`, htmlOutput, 'utf-8');