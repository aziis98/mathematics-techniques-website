#!/bin/sh

inotifywait -m -e close_write src | 
	while read mod_path _ file; do 
		echo "[$(date +%T)] '$mod_path$file' modified"
		# Generate static files
		cp src/icon.png out/icon.png
		cp src/style.css out/style.css
		cp src/index.js out/index.js
		cp src/home.html out/index.html
		# Generate HTML from YAML templates
		# ./build.js src/algebra-1.yaml
		# ./build.js src/eps.yaml
		# ./build.js src/analisi-2.yaml
		# ./build.js src/fisica-3.yaml
	done
